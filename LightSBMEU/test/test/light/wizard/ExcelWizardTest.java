package test.light.wizard;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.javaforever.poitranslator.core.ProjectExcelWorkBook;
import org.junit.Test;
import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.Project;
import org.light.wizard.ExcelWizard;

public class ExcelWizardTest {

	//@Test
	public void testOutputExcelWorkbook() throws Exception {
		String outputFolder = "/home/jerry/Jerry/CodeGenerator/temp/";
		Project project = new Project();
		ExcelWizard.outputExcelWorkBook(project, outputFolder,"test.xls");
	}
	
	//@Test
	public void testOutputExcelWorkbook2() throws Exception {
		String outputFolder = "/home/jerry/Jerry/CodeGenerator/temp/";
		Project project = new Project();
		project.setStandardName("MyTest2");
		Domain d = new Domain();
		d.setStandardName("User");
		Field f0 = new Field("id","long");
		Field f1 = new Field("userName","String");
		Field f2 = new Field("active","boolean");
		Field f3 = new Field("password","String");
		d.setDomainId(f0);
		d.setDomainName(f1);
		d.setActive(f2);
		d.addField(f3);
		
		project.addDomain(d);
		ExcelWizard.outputExcelWorkBook(project, outputFolder,"MyTest2.xls");
	}

	@Test
	public void testOutputExcelWorkbookFromExcel() throws Exception {
		String outputFolder = "/home/jerry/Jerry/CodeGenerator/temp/";
		String sourceFolder = "/home/jerry/Jerry/CodeGenerator/temp/";
	
		InputStream is = new FileInputStream(sourceFolder+"FaithSample.xls");
		POIFSFileSystem fs = new POIFSFileSystem(is);
		HSSFWorkbook wb = new HSSFWorkbook(fs);
	
		ProjectExcelWorkBook pwb = new ProjectExcelWorkBook();
		Project project = pwb.translate(wb,true);
		ExcelWizard.outputExcelWorkBook(project, outputFolder,"FromExcel.xls");
	}
}
