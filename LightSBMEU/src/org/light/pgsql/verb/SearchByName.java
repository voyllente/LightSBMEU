package org.light.pgsql.verb;

import java.util.ArrayList;
import java.util.List;

import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Method;
import org.light.domain.Statement;
import org.light.exception.ValidateException;
import org.light.utils.PgsqlReflector;
import org.light.utils.StringUtil;
import org.light.utils.WriteableUtil;

public class SearchByName extends org.light.verb.SearchByName {

	public SearchByName(Domain domain)  throws ValidateException{
		super(domain);
	}

	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("search" + StringUtil.capFirst(this.domain.getPlural()) + "By"
					+ this.domain.getDomainName().getCapFirstFieldName());
			method.setNoContainer(true);
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(100L, 1, "<select id=\"" + method.getLowerFirstMethodName() + "\" resultMap=\""
					+ this.domain.getLowerFirstDomainName() + "\" parameterType=\"string\">"));
			list.add(new Statement(200L, 2, PgsqlReflector.generateSearchByNameUsingValueStatement(domain)));
			list.add(new Statement(300L, 1, "</select>"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}
}
