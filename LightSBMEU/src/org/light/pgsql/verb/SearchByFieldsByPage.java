package org.light.pgsql.verb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.light.core.Verb;
import org.light.core.Writeable;
import org.light.domain.Domain;
import org.light.domain.Dropdown;
import org.light.domain.Field;
import org.light.domain.JavascriptBlock;
import org.light.domain.JavascriptMethod;
import org.light.domain.Method;
import org.light.domain.Signature;
import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Type;
import org.light.domain.Var;
import org.light.easyui.EasyUIPositions;
import org.light.exception.ValidateException;
import org.light.generator.NamedStatementGenerator;
import org.light.limitedverb.CountSearchByFieldsRecords;
import org.light.utils.DomainTokenUtil;
import org.light.utils.StringUtil;
import org.light.utils.TableStringUtil;
import org.light.utils.WriteableUtil;

public class SearchByFieldsByPage extends org.light.verb.SearchByFieldsByPage {
	public SearchByFieldsByPage(Domain domain) throws ValidateException{
		super(domain);
	}
	
	@Override
	public Method generateDaoImplMethod() throws Exception {
		if (this.denied)
			return null;
		else {
			Method method = new Method();
			method.setStandardName("search" + StringUtil.capFirst(this.domain.getPlural()) + "ByFieldsByLimit");
			method.setNoContainer(true);
			List<Writeable> list = new ArrayList<Writeable>();
			list.add(new Statement(100L, 1, "<select id=\"" + method.getLowerFirstMethodName() + "\" resultMap=\""
					+ this.domain.getLowerFirstDomainName() + "\">"));
			list.add(new Statement(200L, 2, "select " + DomainTokenUtil.generateTableCommaFields(domain) + " from "
					+ domain.getDbPrefix() + TableStringUtil.domainNametoTableName(domain)));
			list.add(new Statement(300L, 2, "where 1=1 "));
			long serial = 400L;
			Set<Field> fields = this.domain.getFieldsWithoutId();
			for (Field f : fields) {
				if (f.getFieldType().equalsIgnoreCase("string")) {
					list.add(new Statement(serial, 2,
							"<if test=\"" + this.domain.getLowerFirstDomainName() + "." + f.getLowerFirstFieldName()
									+ "!=null and " + this.domain.getLowerFirstDomainName() + "."
									+ f.getLowerFirstFieldName() + "!='' \">"));
					list.add(new Statement(serial + 100, 3,
							"and " + DomainTokenUtil.changeDomainFieldtoTableColum(f.getLowerFirstFieldName())
									+ " LIKE '%'|| #{" + this.domain.getLowerFirstDomainName() + "."
									+ f.getLowerFirstFieldName() + "} || '%'"));
				} else {
					list.add(new Statement(serial, 2, "<if test=\"" + this.domain.getLowerFirstDomainName() + "."
							+ f.getLowerFirstFieldName() + "!=null\">"));
					list.add(new Statement(serial + 100, 3,
							"and " + DomainTokenUtil.changeDomainFieldtoTableColum(f.getLowerFirstFieldName()) + " = #{"
									+ this.domain.getLowerFirstDomainName() + "." + f.getLowerFirstFieldName() + "}"));
				}
				list.add(new Statement(serial + 200, 2, "</if>"));
				serial += 300L;
			}
			 if (this.domain.getDomainId()!=null) list.add(new Statement(serial + 50L, 2, " order by " + StringUtil.changeDomainFieldtoTableColum(domain.getDomainId().getFieldName()) + " asc "));
			 list.add(new Statement(serial+70L, 2, "limit #{limit} offset #{start}"));
			list.add(new Statement(serial + 100L, 1, "</select>"));
			method.setMethodStatementList(WriteableUtil.merge(list));
			return method;
		}
	}
}
