package org.light.generator;

import org.light.domain.Domain;
import org.light.exception.ValidateException;

public class Struts2JspTemplateFactory {
	public static JspTemplate getInstance(String type,Domain domain) throws ValidateException {
		switch (type) {
			case "grid" : return new GridJspTemplate(domain);
			case "pagingGrid" : return new PagingGridJspTemplate(domain);
			case "onlyloginindex":
						  return new OnlyLoginIndexJspTemplate();
			case "struts2JsonPagingGrid": return new JsonPagingGridJspTemplate(domain);
			
			default		: return null;
		}
	}
}
