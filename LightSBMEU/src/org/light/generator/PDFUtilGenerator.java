package org.light.generator;

import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Util;

public class PDFUtilGenerator extends Util{
	public PDFUtilGenerator(){
		super();
		super.fileName = "POIExcelUtil.java";
	}
	
	public PDFUtilGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "PDFUtil.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StatementList sList = new StatementList();
		sList.add(new Statement(1000L,0, "package "+this.packageToken+".utils;"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,0,"import java.io.OutputStream;"));
		sList.add(new Statement(4000L,0,"import java.util.List;"));
		sList.add(new Statement(5000L,0,""));
		sList.add(new Statement(6000L,0,"import com.itextpdf.text.BaseColor;"));
		sList.add(new Statement(7000L,0,"import com.itextpdf.text.Document;"));
		sList.add(new Statement(8000L,0,"import com.itextpdf.text.Font;"));
		sList.add(new Statement(9000L,0,"import com.itextpdf.text.PageSize;"));
		sList.add(new Statement(10000L,0,"import com.itextpdf.text.Paragraph;"));
		sList.add(new Statement(11000L,0,"import com.itextpdf.text.Rectangle;"));
		sList.add(new Statement(12000L,0,"import com.itextpdf.text.pdf.BaseFont;"));
		sList.add(new Statement(13000L,0,"import com.itextpdf.text.pdf.PdfPCell;"));
		sList.add(new Statement(14000L,0,"import com.itextpdf.text.pdf.PdfPTable;"));
		sList.add(new Statement(15000L,0,"import com.itextpdf.text.pdf.PdfWriter;"));
		sList.add(new Statement(16000L,0,""));
		sList.add(new Statement(17000L,0,"public final class PDFUtil {"));
		sList.add(new Statement(18000L,1,"public static void exportPDF(OutputStream out, String sheetName, List<String> headers, List<List<String>> contents)"));
		sList.add(new Statement(19000L,3,"throws Exception {"));
		sList.add(new Statement(20000L,2,""));
		sList.add(new Statement(21000L,2,"BaseFont bfChinese = BaseFont.createFont(\"STSong-Light\", \"UniGB-UCS2-H\", BaseFont.NOT_EMBEDDED);"));
		sList.add(new Statement(22000L,2,"Font fontChinese = new Font(bfChinese, 12, Font.NORMAL);"));
		sList.add(new Statement(23000L,2,"Font fontChineseBold = new Font(bfChinese, 12, Font.BOLD);"));
		sList.add(new Statement(24000L,0,""));
		sList.add(new Statement(25000L,2,"Rectangle rectPageSize = new Rectangle(PageSize.A4);"));
		sList.add(new Statement(26000L,0,""));
		sList.add(new Statement(27000L,2,"Document document = new Document(rectPageSize, 50, 50, 50, 50);"));
		sList.add(new Statement(28000L,2,"if (headers.size() > 10) {"));
		sList.add(new Statement(29000L,3,"document.setPageSize(rectPageSize.rotate());"));
		sList.add(new Statement(30000L,2,"}"));
		sList.add(new Statement(31000L,2,"PdfWriter.getInstance(document, out);"));
		sList.add(new Statement(32000L,2,"document.open();"));
		sList.add(new Statement(33000L,2,""));
		sList.add(new Statement(34000L,2,"BaseColor headback = new BaseColor(180,180,180);"));
		sList.add(new Statement(35000L,2,"BaseColor whiteback = new BaseColor(255,255,255);"));
		sList.add(new Statement(36000L,0,""));
		sList.add(new Statement(37000L,2,"PdfPTable table = new PdfPTable(headers.size());"));
		sList.add(new Statement(38000L,2,"writeRow(table, fontChineseBold, headback, headers);"));
		sList.add(new Statement(39000L,0,""));
		sList.add(new Statement(40000L,2,"for (List<String> data : contents) {"));
		sList.add(new Statement(41000L,3,"writeRow(table, fontChinese, whiteback,data);"));
		sList.add(new Statement(42000L,2,"}"));
		sList.add(new Statement(43000L,2,"document.add(table);"));
		sList.add(new Statement(44000L,2,"document.close();"));
		sList.add(new Statement(45000L,1,"}"));
		sList.add(new Statement(46000L,0,""));
		sList.add(new Statement(47000L,1,"protected static void writeRow(PdfPTable table, Font fontChinese, BaseColor color,List<String> data) throws Exception{"));
		sList.add(new Statement(48000L,2,"for (int j = 0; j < data.size(); j++) {"));
		sList.add(new Statement(49000L,3,"PdfPCell cell = new PdfPCell();"));
		sList.add(new Statement(50000L,3,"cell.setBackgroundColor(color);"));
		sList.add(new Statement(51000L,3,"cell.addElement(new Paragraph(data.get(j),fontChinese));"));
		sList.add(new Statement(52000L,3,"table.addCell(cell);"));
		sList.add(new Statement(53000L,2,"}"));
		sList.add(new Statement(54000L,1,"}"));
		sList.add(new Statement(55000L,0,"}"));
		return sList.getContent();
	}

}
