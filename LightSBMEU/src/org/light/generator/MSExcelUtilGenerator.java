package org.light.generator;

import org.light.domain.Statement;
import org.light.domain.StatementList;
import org.light.domain.Util;

public class MSExcelUtilGenerator extends Util{
	public MSExcelUtilGenerator(){
		super();
		super.fileName = "MSExcelUtil.java";
	}
	
	public MSExcelUtilGenerator(String packageToken){
		super();
		this.setPackageToken(packageToken);
		super.fileName = "MSExcelUtil.java";
	}
	
	@Override
	public void setPackageToken(String packageToken) {
		this.packageToken = packageToken;
	}
	
	@Override
	public String generateUtilString() {
		StatementList sList = new StatementList();
		sList.add(new Statement(500L,0, "package "+this.packageToken+".utils;"));
		sList.add(new Statement(1000L,0,"public class MSExcelUtil {"));
		sList.add(new Statement(2000L,0,""));
		sList.add(new Statement(3000L,1,"public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256;"));
		sList.add(new Statement(4000L,1,"public static final int UNIT_OFFSET_LENGTH = 7;"));
		sList.add(new Statement(5000L,1,"public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };"));
		sList.add(new Statement(6000L,0,""));
		sList.add(new Statement(7000L,1,"/**"));
		sList.add(new Statement(8000L,1,"* pixel units to excel width units(units of 1/256th of a character width)"));
		sList.add(new Statement(9000L,1,"*"));
		sList.add(new Statement(10000L,1,"* @param pxs"));
		sList.add(new Statement(11000L,1,"* @return"));
		sList.add(new Statement(12000L,1,"*/"));
		sList.add(new Statement(13000L,1,"public static short pixel2WidthUnits(int pxs) {"));
		sList.add(new Statement(14000L,2,"short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH));"));
		sList.add(new Statement(15000L,2,"widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];"));
		sList.add(new Statement(16000L,2,"return widthUnits;"));
		sList.add(new Statement(17000L,1,"}"));
		sList.add(new Statement(18000L,0,""));
		sList.add(new Statement(19000L,1,"/**"));
		sList.add(new Statement(20000L,1,"* excel width units(units of 1/256th of a character width) to pixel units"));
		sList.add(new Statement(21000L,1,"*"));
		sList.add(new Statement(22000L,1,"* @param widthUnits"));
		sList.add(new Statement(23000L,1,"* @return"));
		sList.add(new Statement(24000L,1,"*/"));
		sList.add(new Statement(25000L,1,"public static int widthUnits2Pixel(int widthUnits) {"));
		sList.add(new Statement(26000L,2,"int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH;"));
		sList.add(new Statement(27000L,2,"int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR;"));
		sList.add(new Statement(28000L,2,"pixels += Math.round(offsetWidthUnits"));
		sList.add(new Statement(29000L,4,"/ ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));"));
		sList.add(new Statement(30000L,0,""));
		sList.add(new Statement(31000L,2,"return pixels;"));
		sList.add(new Statement(32000L,1,"}"));
		sList.add(new Statement(33000L,0,"}"));
		return sList.getContent();
	}

}
