package org.light.domain;

public class MenuItem implements Comparable<MenuItem>{
	protected String url;
	protected String label;
	protected String standardName;
	public MenuItem(String url,String standardName,String label){
		super();
		this.url = url;
		this.standardName = standardName;
		this.label = label;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	@Override
	public int compareTo(MenuItem o) {
		return this.standardName.compareTo(o.getStandardName());
	}
	public String getStandardName() {
		return standardName;
	}
	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}
	
	public String getText() {
		if (this.label != null && !this.label.equals(""))
			return this.label;
		else
			return this.standardName;
	}
}
