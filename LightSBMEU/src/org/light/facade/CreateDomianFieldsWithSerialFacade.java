package org.light.facade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.light.domain.Domain;
import org.light.domain.Field;
import org.light.domain.ManyToMany;
import org.light.domain.Project;
import org.light.utils.StringUtil;
import org.light.wizard.ProjectWizard;

import net.sf.json.JSONObject;

/**
 * 
 * @author Jerry Shen 2021.1.29
 *
 */
public class CreateDomianFieldsWithSerialFacade extends HttpServlet {
	private static final long serialVersionUID = -598489528766854598L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateDomianFieldsWithSerialFacade() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain;charset=UTF-8");
		PrintWriter out = response.getWriter();
		int serial = Integer.valueOf(StringUtil.nullTrim(request.getParameter("serial")));		
		String domainid = StringUtil.nullTrim(request.getParameter("domainid"));
		String domainidtype = StringUtil.nullTrim(request.getParameter("domainidtype"));		
		String domainidlength = StringUtil.nullTrim(request.getParameter("domainidlength"));
		String domainidlabel = StringUtil.nullTrim(request.getParameter("domainidlabel"));
		String domainiddeny = StringUtil.nullTrim(request.getParameter("domainiddeny"));
		String domainname = StringUtil.nullTrim(request.getParameter("domainname"));
		String domainnametype = StringUtil.nullTrim(request.getParameter("domainnametype"));
		String domainnamelength = StringUtil.nullTrim(request.getParameter("domainnamelength"));
		String domainnamelabel = StringUtil.nullTrim(request.getParameter("domainnamelabel"));
		String domainnamedeny = StringUtil.nullTrim(request.getParameter("domainnamedeny"));
		String activefield = StringUtil.nullTrim(request.getParameter("activefield"));
		String activefieldtype = StringUtil.nullTrim(request.getParameter("activefieldtype"));
		String activefieldlength = StringUtil.nullTrim(request.getParameter("activefieldlength"));
		String activefieldlabel = StringUtil.nullTrim(request.getParameter("activefieldlabel"));
		String activefielddeny = StringUtil.nullTrim(request.getParameter("activefielddeny"));
		
		String [] fieldcatlog = request.getParameterValues("fieldcatlog[]");
		String [] field = request.getParameterValues("field[]");
		String [] fieldtype = request.getParameterValues("fieldtype[]");
		String [] fieldlength = request.getParameterValues("fieldlength[]");
		String [] fieldlabel = request.getParameterValues("fieldlabel[]");
		
		
		try {
			Domain domain0 = new Domain();
			if (ExcelWizardFacade.getInstance()!=null&&ExcelWizardFacade.getInstance().getDomains()==null) {
				ExcelWizardFacade.getInstance().addDomain(domain0);
			}
			if (ExcelWizardFacade.getInstance()!=null && ExcelWizardFacade.getInstance().getDomains()!=null && ExcelWizardFacade.getInstance().getDomains().size()>serial) {
				domain0 = ExcelWizardFacade.getInstance().getDomains().get(Integer.valueOf(serial));
			}
			Field fdomainid;
			if (!"true".equalsIgnoreCase(domainiddeny)) {
				fdomainid = new Field(domainid,domainidtype);
				fdomainid.setLengthStr(domainidlength);
				fdomainid.setLabel(domainidlabel);
				fdomainid.setSerial(-300L);
				domain0.setDomainId(fdomainid);				
			}
			Field fdomainname;
			if (!"true".equalsIgnoreCase(domainnamedeny)) {
				fdomainname = new Field(domainname,domainnametype);
				fdomainname.setLengthStr(domainnamelength);
				fdomainname.setLabel(domainnamelabel);
				fdomainname.setSerial(-200L);
				domain0.setDomainName(fdomainname);
			}
			Field factive;
			if (!"true".equalsIgnoreCase(activefielddeny)) {
				factive = new Field(activefield,activefieldtype);
				factive.setLengthStr(activefieldlength);
				factive.setLabel(activefieldlabel);
				factive.setSerial(-100L);
				domain0.setActive(factive);
			}
			if (fieldcatlog!=null) {
				for (int i=0;i<fieldcatlog.length;i++) {
					Map<String,String> params = new TreeMap<>();
					params.put("fieldcatlog",fieldcatlog[i]);
					params.put("fieldname",field[i]);
					params.put("fieldtype",fieldtype[i]);
					params.put("fieldlength",fieldlength[i]);
					params.put("fieldlabel",fieldlabel[i]);
					params.put("serial",""+(i*100));
					if ("manytomany".equals(fieldcatlog[i])) {
						ManyToMany mtm =  ProjectWizard.createNewManyToMany(domain0, params);
						domain0.addManyToMany(mtm);
					}else {
						Field f = ProjectWizard.createNewField(params);
						domain0.addField(f);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		try {
			Map<String, Object> result = new TreeMap<String, Object>();
			result.put("success", true);
			result.put("data", null);
			out.print(JSONObject.fromObject(result));
		} catch(Exception e) {
			e.printStackTrace();
		}
	
	}
}
