package org.light.dao;

import org.light.domain.Naming;

public interface UtilDao {
	public String generateUtilString(Naming naming, String standardName) throws Exception;

}
