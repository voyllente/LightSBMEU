# 第三代动词算子式代码生成器：光SBMEU版


## 本代码生成器支持前后端分离界面了
## 开发者手册已在本站附件处公布
## 让代码生成器成为大家开发Java程序的一项优势
## 2.2智慧版本尝鲜版6公布，支持向导界面操作
## 智慧之光已支持PostgreSQL和即将支持片类型

### 引子

传说在阿拉伯半岛一个神秘的月亮山洞里，有一盏神奇的神灯，它可以满足您三个愿望，夜之精灵守护着这盏神灯，和洞里无数的其他珍宝。此处并非任何人的财产，勇敢的人才是神灯的主人。

年轻的程序员阿拉丁找到了这个山洞，在洞穴幽蓝的微光下，他摩擦了这盏神灯，于是，夜之精灵退后，一位灯神出现了，他宣称可以满足阿拉丁三个愿望，任何愿望。于是，阿拉丁提出了他的第一个愿望："给我一台支持Excel的Apple I。"

灯神为难了:"有没有搞错，那时候有没有Excel?"

"我不管，您不是万能的灯神吗？"

"那好吧，这些研发工作我替乔布斯做了。”

于是，第二个愿望："给我一套Excel模板。"

最后的愿望是：”生成一套管理系统！"

![输入图片说明](https://images.gitee.com/uploads/images/2020/0507/112902_e808911e_1203742.jpeg "ald.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0507/112920_9296072d_1203742.jpeg "ald2.jpg")

### 简介 
第三代动词算子式代码生成器：光SBMEU版，采用Maven, EasyUI,SpringBoot 2, ＭyBatis, MariaDB 技术栈

欢迎大家使用由无垠式，和平之翼和光三代动词算子式代码生成器组成的动词算子式代码生成器阵列，在我的码云站点[https://gitee.com/jerryshensjf/](https://gitee.com/jerryshensjf/)大家可以找到这些代码生成器。把他们统统部署在Tomcat中，您可以获得超过600N的代码变形能力。

### 介绍视频地址

B站地址：
[https://www.bilibili.com/video/BV1uD4y1977o](https://www.bilibili.com/video/BV1uD4y1977o)

### 版本情况

#### 开发中版本：光SBMEU 2.2.0 版　版本代号 Intelligence 智慧 进展情况

智慧已经发布了第六个尝鲜版，请在本站附件处下载。尝鲜版五新支持PostgreSQL数据库。从此，光正式支持MariaDB,Oracle,PostgreSQL三种数据库。
同时，尝鲜版六改进了字段排序，和您输入的顺序一致，并有其他一些错误修正。

请去[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)　下载

B站智慧新版赏析视频：[https://www.bilibili.com/video/BV1rX4y157fC/](https://www.bilibili.com/video/BV1rX4y157fC/)

![输入图片说明](https://images.gitee.com/uploads/images/2020/1027/173527_08f973e4_1203742.jpeg "int.jpeg")

#### 目前开发重点

智慧已启动开发，先导功能是可以注入的空方法和新的数据类型Image,decimal,currency,time,date和datetime。

目前的开发重点是：支持图片类型

已支持第三种数据库: PostgreSQL

### 已完成的特性

智慧将新增Excel模板向导，方便初学者的使用。

智慧新增了高级定制功能，您可以使用此功能生成您需要的组件。

目前，高级定制功能已完成开发，可以下载使用在开发中的智慧的版本使用。

第四个尝鲜版在前面版基础上有多处更新，尤其是对导出Excel模板的格式化更加一致，美观。

功能截图：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0203/214713_437b09ac_1203742.png "ding.png")

Excel模板向导1.0 Wrap Engine Core【曲速引擎核心】已完成，已实现Excel模板格式化，模板和模板向导双向反射，示例一键解析，上传模板解析，向导导出模板和直接代码生成等一系列先进强大的功能。

Excel模板向导1.0完成了系统的闭合。更先进的特性将出现在模板向导1.2 Rage of Khan　【可汗之怒】中。

现在的模板向导截图：

项目设置：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0215/161739_1f451f41_1203742.png "wz.png")

域对象设置：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0215/161801_0818d485_1203742.png "wzd.png")


### 已发布代码生成器稳定版本更新
已释出光2.1.0 Beta6信念版本。新增智能替换工具，并更新了文档，为了方便初学者，新增了空白Excel模板的下载。

为了配合SpringCloud　Ｈ版，光生成的代码生成物的SpringBoot升级到2.2.2

### 光的SpringBoot,Maven版Java通用代码生成器灯公布，支持源码运行

此版本为大家想要的SpringBoot,Maven版本，从光2.2尝鲜版２分支，已经可以通过源码编译运行。

项目地址：
[https://gitee.com/jerryshensjf/Lamp](https://gitee.com/jerryshensjf/Lamp)

### 光之翼版本公布
光之翼java通用代码生成器1.0.0 Beta３版已公布。Swing独立版，采用光SBMEU版2.1.0 Beta3信念版本和时空之门前端代码生成器4.5.0Beta10版本的生成引擎。

请去[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)　下载

光之翼代码生成器为可运行jar，双击即可运行。

光之翼项目地址：[https://gitee.com/jerryshensjf/LightWing](https://gitee.com/jerryshensjf/LightWing)

### 注意

因为眼疾，我不得不把动词算子式代码生成器的研发工作停止在目前的状态。研发工作已经延续了七年，有很多成就，也有很多遗憾，更有很多已规划但却从未实现的功能。现在，我把这些内容功能公布出来。也许，呼唤一位英雄，也许是几位。

至尊宝，这是您的箍和屠龙宝刀。接好了，屠龙刀乃是宝物，不要说砸到小朋友，砸到了花花草草也是不好的。

我将在开源中国博客中公布所有未完成的功能构想。并基于光1.5.0的代码编写《开发者手册》（《黑客手册》）。

《开发者手册》正在编写之中，已可在本站附件处下载，持续更新中。
[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)

可在本站附件处下载各个版本的光。另外，附件中有多个光2.1 信念的操作视频，解说语言有中文和英文（不同视频），欢迎使用。
[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)

### 项目图片：光
![输入图片说明](https://images.gitee.com/uploads/images/2019/0209/144015_1af4fc3c_1203742.jpeg "light.jpg")

### 荣誉
本代码生成器荣获2020年度开源中国“最积极运营项目”奖，有图为证：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/105748_6ea77178_1203742.jpeg "Photo_0118_1a.jpg")

### 历史版本情况

#### 现在的稳定版本：光SBMEU 2.1.0 版　版本代号Faith 信念

光2.1.0 Beta6信念版本。新增智能替换工具，并更新了文档，为了方便初学者，新增了空白Excel模板的下载。

光2.1.0 Beta3信念版本。为了配合SpringCloud　Ｈ版，光生成的代码生成物的SpringBoot升级到2.2.2

信念Beta2版，此版本包括５个新功能：Language,Schema, SQLLength，下划线分隔的字段名，字段否定。注入空单域动词功能尚待实现。其他功能将延续至光2.2　Intelligence 智慧版本实现。有些功能可能进一步延后。

Beta2版的最大优势是三大变形功能群齐备，并可以联合使用。Beta2版在Beta版基础上，排除了很多缺陷才达成这一目的。

三大变形功能群如下：

#### 动态椰子树功能群：
代码生成物的文件夹结构和域对象和控制器的后缀可配置。

B站介绍视频：[https://www.bilibili.com/video/BV1v54y1R7Tw/](https://www.bilibili.com/video/BV1v54y1R7Tw/)

#### 动词否定功能群：
可以去除您不需要的功能

B站介绍视频：[https://www.bilibili.com/video/BV1Mt4y1e7AK/](https://www.bilibili.com/video/BV1Mt4y1e7AK/)

#### 字段否定功能群：

域对象三个特殊字段序号，域对象名字和活跃字段不再必选，而是可以缺失或者部分缺失，但是缺失这些字段意味着功能上有相应的缺失

B站介绍视频：[https://www.bilibili.com/video/BV1uZ4y1V7dz/](https://www.bilibili.com/video/BV1uZ4y1V7dz/)

![输入图片说明](https://images.gitee.com/uploads/images/2020/0511/165702_b6c3a517_1203742.jpeg "faith.jpg")

#### 信念的新功能介绍

#### 英文界面

项目页设置language为English,生成界面即为英文界面。

#### DBTools模式

项目页设置schema为DBTools,生成结果为各种SQL语句，并不再生成前端和后端项目。

#### 字段长度

现在，字符串型的字段可以设置字段长度。

#### 下划线分隔的字段

Domain页可以设置字段名，域对象名，复数为下划线分割的名字。这些字段将自动转化为驼峰样式，规则为有下划线的名字不区分大小写，紧接下划线的字母会转化成大写字母。域名，域复数名为大写起头，字段名为小写起头。

#### 字段否定功能群

系统要求每个域对象有三个特殊字段，domainId,domainName和activeField,现在，可以不包含这些字段，但是，会有功能损失。三个字段不全的域对象被成为遗留域对象，不可以参与多对多关系，不能成为一对多关系的目标对象，对Grid页面，随所缺字段，一部分动词会被自动否定。

#### 信念及未来的版本计划

1. SQLlength特性， SQLType的缩减版[已实现]
1. Calc动词
1. default和default+，default-动词堆栈
1. decimal，date，time和datetime数据类型
1. image数据类型
1. BootAuth登录模块
1. 前端项目相应更新
1. 全数据策略，生成测试案例
1. 可以关闭打开的自动生成的注释
1. 可以注入含可变参数列表单域空白动词，编译无错
1. 下划线分割的字段名自动解析[已实现]
1. DBTools模式，生成SQL Insert,Update,Delete语句，包含Create DB, Create table 语句。[已实现]
1. 支持英语界面，即language选项[已实现]
1. 字段否定功能，domainId,domainName,activeField三者可以缺失，但会付出功能缺失的代价[已实现]

#### 未来的版本代号
光2.2智慧　Intelligence

![输入图片说明](https://images.gitee.com/uploads/images/2020/1027/173527_08f973e4_1203742.jpeg "int.jpeg")

光2.3文明 Civilization

![输入图片说明](https://images.gitee.com/uploads/images/2020/1027/173606_bf7c4080_1203742.jpeg "cov4.jpg")

光3.0黎明 Dawn

![输入图片说明](https://images.gitee.com/uploads/images/2020/1027/173642_b671697d_1203742.jpeg "dawn2.jpeg")

#### 历史版本代号

#### 光SBMEU 2.0 版　版本代号Insight 内省
![输入图片说明](https://images.gitee.com/uploads/images/2020/0402/174941_713c2668_1203742.jpeg "Insight2.jpg")

主要包含动态椰子树式的目录结构，动态域对象和Controller后缀，域对象个性化表名前缀和动词否定四个功能。

原来，动词算子式代码生成器的目录结构是椰子树式的，就是路径前缀是可变的，但是像Service和Dao等等的子文件夹是固定的，现在，这些子文件夹也是动态的，所以称为动态椰子树式的文件夹。

动态域对象和Controller后缀允许产生各种风格的代码，以适应不同项目组的需要。

域对象个性化表名前缀允许一个项目拥有多种表名前缀，对有些项目组而言，这是他们喜欢的风格。

动词否定的含义是您可以在Excel模板中的域对象页中否定掉您不需要的动词。这样，就不需要手工删除了，大大节省了工作量，这是我很自豪的功能，我没有做功能的加法，而是使用了减法，这是非常实用和新颖的。

Insight的Beta3版已经实现了动词否定功能，从此，您可以灵活的定义对象的操作。如果某对象不需要删除功能，您只需要否定Delete功能，相关代码就不会在代码生成物中出现了。

#### 内省(Insight)的Beta3版

内省的Beta3版已公布。请使用示例DynamicSample，DynamicSample2和DynamicOracleEn作为动态椰子树功能样例测试。请使用DenySample和DenyOracleEn作为动词否定的样例测试。可在本站附件处下载Beta3版。
[https://gitee.com/jerryshensjf/LightSBMEU/attach_files](https://gitee.com/jerryshensjf/LightSBMEU/attach_files)

现在，Beta3版包含动态椰子树式的目录结构，域对象和Controller后缀可配置和域对象可配置表名前缀和动词否定四大功能。

在不同项目组的实践中，有些代码的细节是不一致的，比如，域对象有人喜欢不带后缀，有人喜欢使用Entiy或PO，有人喜欢后缀Dto。路径有人喜欢使用dao,daoimpl,service,serviceimpl,controller,而有人喜欢使用dao,dao.impl,service,service.impl,controller这些变体，内省都支持。你甚至可以规定Controller使用Facade或者Adapter后缀，并定义相关的Controller路径，内省都可以准确的生成无错的代码。

#### 内省(Insight)的Beta3版的动词否定功能

有了动词否定功能，您可以否定掉不需要的功能，但是注意，动词间有依赖关系，比如，您否定了Clone功能，却没有否定CloneAll功能，这时，代码生成器会报错，原因是CloneAll依赖Clone动词完成每一个Clone动作，否定CloneAll而不否定Clone是可以的，反之则有编译错误。

特别的，Beta3版提供了便利的max和max+功能。max功能是最大限度的否定，即除了ListActive,SearchByFieldsByPage和FindById三个默认动词外全部否定。
max+功能是一种便利的约定，单独的max+含义和max一样，但是max+,Add,Update 的含义是除了三个默认动词外还有Add和Update两个动词不被否定，即有5个动词，即是max+后面的动词列表不是默认的减法，而是加法。
Beta3版提供了动词否定的在线文档。请大家参考。

#### 动词否定效果图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0423/100234_6754e18d_1203742.png "denyVerb.png")
可以看见按钮条在不同配置下的功能伸缩。

#### 光SBMEU 1.5版　版本代号Kama 顿悟
![输入图片说明](https://images.gitee.com/uploads/images/2019/0812/145356_1c8effde_1203742.jpeg "kama.jpg")

### 动词算子式代码生成器的基本设计

简单说一下，我的代码生成器有三层，项目，棱柱和域对象。有大概20个动词算子，或者称为泛型动词算子。比如说，新增就是一个动词算子，编辑是另外一个动词算子。您可以选择一个域对象上面使用的动词算子，当然要遵守一些依赖关系。还可以在域对象之间建立一对多和多对多关系。这样，一个正确设置的Excel工作簿会被编译成项目，这就是动词算子式代码生成器的基本逻辑。 

### 功能亮点截图

#### Excel和数据库的新玩法
其实，还可以这样用代码生成器，把Excel里的数据整成代码生成器的模板格式，填好数据，生成项目，操作数据完成工作，再利用代码生成物的Excel导出功能得到加工好的数据，非常方便，使您拥有了在Excel和数据库之间自由迁徙的能力。 

#### 前后端分离项目自动生成
第三代动词算子式代码生成器：光SBMEU版已支持Vue+ElementUI前后端分离项目自动生成。您只需要定义一个后端项目的Excel模板，即可一次生成后端项目和与之配套的前端项目，非常强大，您值得一试。

#### 同时生成前端后端项目的功能截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1107/170008_986cc418_1203742.png "FaithWithResults.png")

#### 前端项目运行使用方法。
前端项目的使用：首先使用光SBMEU版，生成后端项目和对应的前端项目。运行后端项目。注意，启动Application.java类启动SpringBoot的后端项目，您需要首先使用Ｍaven将例程编译。

将前端项目解压。如果没有安装Nodejs，请先安装。在解压的前端界面文件夹内运行 npm install命令。运行好后运行npm run dev

一切就绪后访问 http://localhost:8000/ 即可使用此示例。

#### 代码生成物导出功能截图

导出Excel：

![输入图片说明](https://images.gitee.com/uploads/images/2019/1224/131855_9985b2f6_1203742.png "output_excel.png")

导出PDF：

![输入图片说明](https://images.gitee.com/uploads/images/2019/1224/131915_bc0e252a_1203742.png "output_pdf.png")

#### 开发者手册（“黑客手册”）截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/190512_ae13696a_1203742.png "hacker.png")

#### Spring Boot启动，有图为证：

![输入图片说明](https://gitee.com/uploads/images/2019/0503/132703_854df33d_1203742.png "springboot.png")

#### 相关技术博客
地址：[https://my.oschina.net/jerryshensjf](https://my.oschina.net/jerryshensjf)

#### 技术博客截图：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/201120_eae9fbef_1203742.png "blog.png")

### 前端项目截图：
登录：

![登录](https://images.gitee.com/uploads/images/2019/0415/214758_8c47b686_1203742.png "vue_login.png")

Grid:

![Grid](https://images.gitee.com/uploads/images/2019/0415/214815_c2dfdd1e_1203742.png "vue_bonuses.png")

多对多：

![多对多](https://images.gitee.com/uploads/images/2019/0415/220549_b19d2ca4_1203742.png "Vue_mtm.png")

编辑，下拉列表：

![输入图片说明](https://images.gitee.com/uploads/images/2019/0416/085420_45584d04_1203742.png "vue_update_dropdown.png")

## 光SMBEU版截图
光取消了对SGS脚本的支持，统一使用电子表格来生成代码，支持.xls格式，可以使用MS Office, WPS Office或Libre Office中的电子表格软件，都要存成.xls格式。

### Excel模板截图

下图是生成代码的Excel模板示例，Linux下使用WPS Office的电子表格软件
![输入图片说明](https://gitee.com/uploads/images/2019/0502/211142_aebb6cb2_1203742.png "lt_bng_wps_excel_project.png")

![输入图片说明](https://gitee.com/uploads/images/2019/0502/211154_b46b860d_1203742.png "lt_bng_wps_excel_item.png")

### 代码生成器截图：

#### Excel生成界面

![输入图片说明](https://images.gitee.com/uploads/images/2020/1005/150659_a51a115d_1203742.png "Faith.png")

#### 文档

![输入图片说明](https://gitee.com/uploads/images/2019/0504/121841_01dc12cb_1203742.png "lt_bng_doc_new.png")

### 后端项目代码生成物截图：

主页：

![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/104535_df69a6a2_1203742.png "home.png")

列表页面:

![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/104601_3a293615_1203742.png "grid.png")

更新页面：

![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/104617_26ff7fb0_1203742.png "update.png")

一对多下拉列表:

![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/104650_0f23ae48_1203742.png "grid_dropdoown.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/104714_fd88615d_1203742.png "update_dropdown.png")

多对多

![输入图片说明](https://images.gitee.com/uploads/images/2019/1223/104734_f361c100_1203742.png "mtm.png")

### 交流QQ群
无垠式代码生成器群 277689737

### 官方配乐：邓紫棋《光年之外》
